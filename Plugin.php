<?php namespace Bitcraft\Seomanager;

use Illuminate\Support\Facades\Event;
use Bitcraft\SeoManager\Components\SeoTags;
use Bitcraft\SeoManager\Models\Settings;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    /**
     * @var bool
     */
    public $elevated = true;

    /**
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'bitcraft.seomanager::lang.plugin.name',
            'description' => 'bitcraft.seomanager::lang.plugin.description',
            'author' => 'bitcraft',
            'icon' => 'icon-search',
        ];
    }

    /**
     * @return void
     */
    public function boot()
    {
        Event::listen('backend.form.extendFieldsBefore', function ($widget) {
            $widget->getController()->addCss('/plugins/bitcraft/seomanager/assets/css/style.css');
            $widget->getController()->addJs('/plugins/bitcraft/seomanager/assets/js/main.js');
        });
    }

    /**
     * @return void
     */
    public function register()
    {
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'bitcraft.seomanager::lang.settings.label',
                'description' => 'bitcraft.seomanager::lang.settings.description',
                'category' => 'bitcraft.seomanager::lang.settings.category',
                'icon' => 'icon-code',
                'class' => Settings::class,
                'order' => 500,
                'permissions' => ['bitcraft.seomanager.access_settings'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'bitcraft.seomanager.access_settings' => [
                'label' => 'bitcraft.seomanager::lang.permissions.settings',
                'tab' => 'bitcraft.seomanager::lang.permissions.tab',
            ],
            'bitcraft.seomanager.change_htaccess' => [
                'label' => 'bitcraft.seomanager::lang.permissions.change_htaccess',
                'tab' => 'bitcraft.seomanager::lang.permissions.tab',
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            SeoTags::class => 'seoTags',
        ];
    }
}
