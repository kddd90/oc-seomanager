<?php

namespace Bitcraft\SeoManager\Models;

use Illuminate\Support\Facades\DB;
use Model;

/**
 * Class SeoTag
 * @package Bitcraft\SeoManager\Models
 */
class SeoTag extends Model
{

    /**
     * @var array
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string
     */
    public $table = 'bitcraft_seomanager_seo_tags';

    /**
     * @var array
     */
    public $translatable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'canonical_url',
        'redirect_url',
        'og_title',
        'og_description',
    ];

    /**
     * @var array
     */
    public $morphTo = [
        'seo_tag' => [],
    ];

    public function encode()
    {
        /* this is where you write the function */
        $array = $this->toArray();
        $translations = [];

        foreach ($this->translations->toArray() as $item) {
            $translations[$item['locale']] = $item;
        }

        $array['translations'] = $translations;
        return $array;
    }

    public function beforeSave() {
        if (($tag = post("SeoTag"))) {
            $this->robot_index = $tag['robot_index'];
            $this->robot_follow = $tag['robot_follow'];
            $this->robot_advanced = $tag['robot_advanced'];
            $this->og_type = $tag['og_type'];
            $this->og_image = $tag['og_image'];
        }
    }

    public function afterSave()
    {
        if (post("RLTranslate")) {
            foreach (post("RLTranslate") as $key => $value) {

                $data = json_encode($value);

                $obj = Db::table("rainlab_translate_attributes")
                    ->where("locale", $key)
                    ->where("model_id", $this->id)
                    ->where("model_type", get_class($this));

                if ($obj->count() > 0) {
                    $obj->update(["attribute_data" => $data]);
                } else {
                    DB::table('rainlab_translate_attributes')->insert([
                        [
                            'locale' => $key,
                            'model_id' => $this->id,
                            'model_type' => get_class($this),
                            'attribute_data' => $data
                        ]
                    ]);
                }
            }
        }
    }

}
