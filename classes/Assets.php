<?php

namespace Bitcraft\SeoManager\Classes;

use Backend\Classes\Controller;

/**
 * Class Assets
 * @package Bitcraft\SeoManager\Classes
 */
class Assets
{

    /**
     * @param  Controller  $controller
     */
    public static function add($controller)
    {
        $controller->addCss('/plugins/bitcraft/seomanager/assets/css/style.css');
        $controller->addJs('/plugins/bitcraft/seomanager/assets/js/main.js');
    }
}
